from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from django_cdstack_deploy.django_cdstack_deploy.models import *
from django_cdstack_models.django_cdstack_models.func import get_or_create_cmdb_host


@api_view(["PUT"])
@permission_classes((permissions.AllowAny,))
def apps(request):
    try:
        tmp_int = int(request.data.get("deployment_id"))
    except ValueError:
        content = {"error": "Instance-ID not found or incorrect!"}
        return Response(content, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

    if request.method == "PUT":
        try:
            instance = CmdbInstance.objects.get(id=request.data.get("deployment_id"))
        except CmdbInstance.DoesNotExist:
            content = {"error": "Instance-ID not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        try:
            api_key = instance.api_key
        except CmdbApiKeyInstance.DoesNotExist:
            content = {"error": "Instance-ApiKey not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        if not api_key.key == request.data.get("deployment_pw"):
            content = {"error": "Instance-ApiKey not found or incorrect!"}
            return Response(content, status=status.HTTP_404_NOT_FOUND)

        if len(request.data.get("fqdn")) <= 3:
            content = {"error": "Hostname invalid / too short!"}
            return Response(content, status=status.HTTP_422_UNPROCESSABLE_ENTITY)

        host = get_or_create_cmdb_host(request.data.get("fqdn"), instance)

        apps_data = request.data.get("apps")

        for app_row in apps_data:
            # skip apps with name null
            if not app_row["app_name"]:
                continue

            app_name_stripped = app_row["app_name"].strip()

            try:
                app = CmdbAppData.objects.get(app_name=app_name_stripped)
            except CmdbAppData.DoesNotExist:
                app = CmdbAppData(app_name=app_name_stripped)
                app.save(force_insert=True)

            try:
                app_relation = CmdbAppRel.objects.get(host=host, app=app)
            except CmdbAppRel.DoesNotExist:
                app_relation = CmdbAppRel(host=host, app=app)
                app_relation.save(force_insert=True)

            # TODO: remove apps that are not present anymore

    else:
        content = "SOMETHING BROKE BADLY"
        return Response(content, status=status.HTTP_501_NOT_IMPLEMENTED)

    content = "OK"

    return Response(content, status=status.HTTP_200_OK)
