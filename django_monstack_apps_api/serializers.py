from rest_framework import serializers

from django_monstack_models.django_monstack_models.models import CollectAppsData


class CollectAppsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CollectAppsData
        fields = ("app_id", "app_name")
